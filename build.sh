#!/bin/bash
set -e
mkdir -p cmakebuild; cd cmakebuild
cmake ..
rm -rf bin/*
make
