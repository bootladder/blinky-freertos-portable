#include "FreeRTOS.h"
#include "task.h"

#include "PortHW.h"

void ToggleLED_Timer(void *pvParameters);

int main(void)
{
    PortHW_Init();

    xTaskCreate(
        ToggleLED_Timer,                 /* Function pointer */
        "Task1",                          /* Task name - for debugging only*/
        configMINIMAL_STACK_SIZE,         /* Stack depth in words */
        (void*) NULL,                     /* Pointer to tasks arguments (parameter) */
        tskIDLE_PRIORITY + 2UL,           /* Task priority*/
        NULL                              /* Task handle */
    );

    vTaskStartScheduler();

    while(1) {
    }
}


void ToggleLED_Timer(void *pvParameters){

  static int toggler = 0;
  while (1) {

    if( 1 == toggler ) {
      PortHW_Led_1_On();
      toggler = 0;
    }
    else {
      PortHW_Led_1_Off();
      toggler = 1;
    }

    /* Blocking Delay */
    vTaskDelay(1000 / portTICK_RATE_MS);
  }
  (void)pvParameters;
}


