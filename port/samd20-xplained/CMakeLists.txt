########################################
# NAME
#######################################
set(TARGET_BASENAME_C blinky-freertos-samd20-xplained)

########################################
# TOOLS
#######################################
set(CMAKE_C_COMPILER arm-none-eabi-gcc)
set(OBJCOPY arm-none-eabi-objcopy )
set(CMAKE_ASM_COMPILER arm-none-eabi-as)
enable_language(ASM)

########################################
# VARIABLE FOR THIS SOURCE DIRECTORY
#######################################
set(THIS_SOURCE_DIR "${CMAKE_SOURCE_DIR}/port/samd20-xplained")

#############################################################
# TARGET SPECIFIC PLUS FREETOS, SOURCES AND INCLUDES
############################################################
include_directories("${FREERTOS_DIR}/portable/GCC/ARM_CM0")
include_directories("${THIS_SOURCE_DIR}")

set(VENDOR_LIBRARY_PATH "${THIS_SOURCE_DIR}/vendor")
include_directories("${VENDOR_LIBRARY_PATH}/samd20_headers")
include_directories("${VENDOR_LIBRARY_PATH}/arm_cmsis_headers")
 

set(FREERTOS_PORT_SOURCE
    "${FREERTOS_DIR}/portable/GCC/ARM_CM0/port.c"
)

set(SOURCES_C
    ${APP_SOURCE_COMMON}
    ${FREERTOS_PORT_SOURCE}
    "${THIS_SOURCE_DIR}/PortHW_samd20.c"
    "${THIS_SOURCE_DIR}/halStartup.c"
    #"${THIS_SOURCE_DIR}/system_stm32f4xx.c"
)
file(GLOB ASM_GLOB
    #"${THIS_SOURCE_DIR}/startup_stm32f4xx.s"
)

#############################################################
# TARGET SPECIFIC FLAGS
############################################################
set(ARCH_ARM_MCPU "-mlittle-endian -mthumb -mcpu=cortex-m0 -mthumb-interwork ")
#set(STM32F4_FLAGS "-DUSE_STDPERIPH_DRIVER               ") 

set(TARGET_C_FLAGS "")
string(APPEND TARGET_C_FLAGS ${ARCH_ARM_MCPU})
string(APPEND TARGET_C_FLAGS "-DFreeRTOS_CPUClockSpeed=1000000")

set(PATH_TO_LINKERSCRIPT_C "${THIS_SOURCE_DIR}/atsamd20j18.ld")
set(TARGET_C_LINK_FLAGS " -T${PATH_TO_LINKERSCRIPT_C}  ")
string(APPEND TARGET_C_LINK_FLAGS ${ARCH_ARM_MCPU})

#############################################################
# CONFIGURE EXECUTABLE
############################################################
add_executable(${TARGET_BASENAME_C}  ${ASM_GLOB} ${SOURCES_C})

C_ConfigureTarget(${TARGET_BASENAME_C})

set_target_properties(${TARGET_BASENAME_C} PROPERTIES LINK_FLAGS ${TARGET_C_LINK_FLAGS} )
set_source_files_properties(${SOURCES_C} PROPERTIES COMPILE_FLAGS ${TARGET_C_FLAGS})
