#include "PortHW.h"
#include "stm32f4xx.h"

int PortHW_Init(void)
{
  RCC->AHB1ENR |= RCC_AHB1ENR_GPIODEN;  // enable the clock to GPIOD
  __asm("dsb");                         // stall instruction pipeline, until instruction completes, as
                                          //    per Errata 2.1.13, "Delay after an RCC peripheral clock enabling"
    GPIOD->MODER = (1 << 26);             // set pin 13 to be general purpose output
  return 0;
}
int PortHW_Led_1_On(void)
{
  GPIOD->ODR ^= (1 << 13);           // Toggle the pin 
  return 0;
}
int PortHW_Led_1_Off(void)
{
  GPIOD->ODR ^= (1 << 13);           // Toggle the pin 
  return 0;
}

