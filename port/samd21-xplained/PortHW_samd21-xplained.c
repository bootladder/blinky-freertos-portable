#include "PortHW.h"
#include "halLed.h"

int PortHW_Init(void)
{
  HAL_LedInit();
  HAL_LedOn(0);
  return 0;
}
int PortHW_Led_1_On(void)
{
  HAL_LedOn(0);
  return 0;
}
int PortHW_Led_1_Off(void)
{
  HAL_LedOff(0);
  return 0;
}

