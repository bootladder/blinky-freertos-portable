#include "PortHW.h"
#include "pio.h"

//D13 = GPIO_PIN(1,27),   //!<PIOB:27
#define PIO_REG PIOB
#define LED_PIN 27
#define PIO_MASK (1 << LED_PIN)


int PortHW_Init(void)
{
  PIO_Configure(PIO_REG,PIO_OUTPUT_1,PIO_MASK,0);
  return 0;
}
int PortHW_Led_1_On(void)
{
  PIO_Set(PIO_REG,PIO_MASK);
  return 0;
}
int PortHW_Led_1_Off(void)
{
  PIO_Clear(PIO_REG,PIO_MASK);
  return 0;
}

