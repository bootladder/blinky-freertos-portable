########################################
# NAME
#######################################
set(TARGET_BASENAME_C blinky-freertos-arduino-due)

########################################
# TOOLS
#######################################
set(CMAKE_C_COMPILER arm-none-eabi-gcc)
set(OBJCOPY arm-none-eabi-objcopy )
set(CMAKE_ASM_COMPILER arm-none-eabi-as)
enable_language(ASM)

########################################
# VARIABLE FOR THIS SOURCE DIRECTORY
#######################################
set(THIS_SOURCE_DIR "${CMAKE_SOURCE_DIR}/port/arduino-due")

#############################################################
# TARGET SPECIFIC PLUS FREETOS, SOURCES AND INCLUDES
############################################################
include_directories("${FREERTOS_DIR}/portable/GCC/ARM_CM3")
include_directories("${THIS_SOURCE_DIR}")

set(VENDOR_LIBRARY_PATH "${THIS_SOURCE_DIR}/vendor")
include_directories("${VENDOR_LIBRARY_PATH}/libsam")
include_directories("${VENDOR_LIBRARY_PATH}/libsam/include")
include_directories("${VENDOR_LIBRARY_PATH}/CMSIS/Device")
include_directories("${VENDOR_LIBRARY_PATH}/CMSIS/Include")
 

set(FREERTOS_PORT_SOURCE
    "${FREERTOS_DIR}/portable/GCC/ARM_CM3/port.c"
)

set(SOURCES_C
    ${APP_SOURCE_COMMON}
    ${FREERTOS_PORT_SOURCE}
    "${THIS_SOURCE_DIR}/PortHW_arduino-due.c"
    "${THIS_SOURCE_DIR}/crt0.c"
    "${THIS_SOURCE_DIR}/vectors.c"
)
file(GLOB VENDOR_GLOB
    "${VENDOR_LIBRARY_PATH}/libsam/source/*.c"
)

#############################################################
# TARGET SPECIFIC FLAGS
############################################################
set(ARCH_ARM_MCPU "-mlittle-endian -mthumb -mcpu=cortex-m3 -mthumb-interwork ")

set(TARGET_C_FLAGS "")
string(APPEND TARGET_C_FLAGS ${ARCH_ARM_MCPU})
string(APPEND TARGET_C_FLAGS "-DFreeRTOS_CPUClockSpeed=4000000 ")
string(APPEND TARGET_C_FLAGS "-D__SAM3X8E__ ")
string(APPEND TARGET_C_FLAGS " -Wfatal-errors")

set(PATH_TO_LINKERSCRIPT_C "${THIS_SOURCE_DIR}/sam3x8e_flash.ld")
set(TARGET_C_LINK_FLAGS " -T${PATH_TO_LINKERSCRIPT_C}  ")
string(APPEND TARGET_C_LINK_FLAGS ${ARCH_ARM_MCPU})

#############################################################
# CONFIGURE EXECUTABLE
############################################################
add_executable(${TARGET_BASENAME_C}  ${VENDOR_GLOB} ${SOURCES_C})

C_ConfigureTarget(${TARGET_BASENAME_C})

set_target_properties(${TARGET_BASENAME_C} PROPERTIES LINK_FLAGS ${TARGET_C_LINK_FLAGS} )
set_source_files_properties(${SOURCES_C} ${VENDOR_GLOB} PROPERTIES COMPILE_FLAGS ${TARGET_C_FLAGS})
