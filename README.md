**This is an exercise in writing portable firmware**  
# What is it:
* Blinks an LED
* Uses FreeRTOS for Task and Timing
* The same app.c is compiled for all targets
* Target specific ports are in port/
  

Targets:
* STM32F4 Discovery
* SAMD20 XPlained
* SAMD21 XPlained
* Arduino Due
  
To add a new target, simply copy some port inside port/ and start replacing files and CMakeLists.txt  

